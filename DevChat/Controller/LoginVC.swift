//
//  LoginVC.swift
//  DevChat
//
//  Created by Ruben Dias on 21/03/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import Foundation

class LoginVC: UIViewController {
    
    @IBOutlet weak var emailField: RoundTextField!
    @IBOutlet weak var passwordField: RoundTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        if let email = emailField.text, let pass = passwordField.text, (email.count > 0 && pass.count > 0) {
            AuthService.instance.login(email: email, password: pass, onComplete: { (errorMessage, user) in
                guard errorMessage == nil else {
                    let alert = UIAlertController(title: "Authentication error", message: errorMessage, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                
                self.dismiss(animated: true, completion: nil)
            })
        } else {
            let alert = UIAlertController(title: "Username and Password required", message: "You must enter both a username and a password to login.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
}
