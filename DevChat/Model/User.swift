//
//  File.swift
//  DevChat
//
//  Created by Ruben Dias on 21/03/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import Foundation

struct User {
    private var _uid: String
    private var _firstName: String
    
    var uid: String {
        return _uid
    }
    
    var firstName: String {
        return _firstName
    }    
    
    init(uid: String, firstName: String) {
        _uid = uid
        _firstName = firstName
    }
}
