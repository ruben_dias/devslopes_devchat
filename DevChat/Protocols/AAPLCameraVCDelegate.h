//
//  NSObject+AAPLCameraVCDelegate.h
//  DevChat
//
//  Created by Ruben Dias on 19/03/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef Header_h
#define Header_h

@protocol AAPLCameraVCDelegate <NSObject>

- (void) shouldEnableRecordUI: (BOOL) enable;
- (void) shouldEnableCameraUI: (BOOL) enable;
- (void) canStartRecording;
- (void) recordingHasStarted;
- (void) videoRecordingComplete: (NSURL*) videoURL;
- (void) videoRecordingFailed;
- (void) snapshotTaken: (NSData*) snapshotData;
- (void) snapshotFailed;

@end

#endif /* Header_h */
