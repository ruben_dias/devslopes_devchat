//
//  AuthService.swift
//  DevChat
//
//  Created by Ruben Dias on 21/03/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import Foundation
import FirebaseAuth

typealias Completion = (_ errMsg: String?, _ data: AnyObject?) -> Void

class AuthService {
    
    private static let _instance = AuthService()
    
    static var instance: AuthService {
        return _instance
    }
    
    func login(email: String, password: String, onComplete: Completion?) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, err) in
            if err != nil {
                if let errorCode = AuthErrorCode(rawValue: err!._code) {
                    if errorCode == .userNotFound {
                        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
                            if error != nil {
                                handleFirebaseError(error: error! as NSError, onComplete: onComplete)
                            } else {
                                if user?.uid != nil {
                                    // store new user in Firebase Database
                                    DataService.instance.saveUser(uid: user!.uid)
                                    
                                    // sign in
                                    Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
                                        if error != nil {
                                            handleFirebaseError(error: error! as NSError, onComplete: onComplete)
                                        } else {
                                            // successfully logged in
                                            onComplete?(nil, user)
                                        }
                                    })
                                }
                            }
                        })
                    } else {
                        handleFirebaseError(error: err! as NSError, onComplete: onComplete)
                    }
                }
            } else {
                // successfully logged in
                onComplete?(nil, user)
            }
        }
    }
}

func handleFirebaseError(error: NSError, onComplete: Completion?) {
    print(error.debugDescription)
    
    if let errorCode = AuthErrorCode(rawValue: error._code) {
        switch errorCode {
            case .invalidEmail :
                onComplete?("Invalid email address.", nil)
                break
            case .wrongPassword :
                onComplete?("Wrong password.", nil)
                break
            case .weakPassword :
                onComplete?("Password must be at least 6 characters long.", nil)
                break
            case .emailAlreadyInUse, .accountExistsWithDifferentCredential:
                onComplete?("Could not create account, email already in use.", nil)
                break
            default:
                onComplete?("Problem with authentication. Please try again.", nil)
                break
        }
    }
}
